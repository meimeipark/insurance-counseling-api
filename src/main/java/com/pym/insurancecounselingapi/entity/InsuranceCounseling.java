package com.pym.insurancecounselingapi.entity;

import com.pym.insurancecounselingapi.enums.DiseaseCategory;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class InsuranceCounseling {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate counselingDay;

    @Column(nullable = false, length = 30)
    private String clientName;

    @Column(nullable = false)
    private LocalDate birthDay;

    @Column(nullable = false, length = 20)
    private String phoneNumber;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private Boolean contract;

    @Column(nullable = false)
    private Double hopeMoney;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private DiseaseCategory diseaseCategory;

    @Column(nullable = false)
    private Boolean differentJoin;
}
