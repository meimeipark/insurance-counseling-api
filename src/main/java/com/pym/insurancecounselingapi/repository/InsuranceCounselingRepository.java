package com.pym.insurancecounselingapi.repository;

import com.pym.insurancecounselingapi.entity.InsuranceCounseling;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceCounselingRepository extends JpaRepository <InsuranceCounseling, Long> {
}
