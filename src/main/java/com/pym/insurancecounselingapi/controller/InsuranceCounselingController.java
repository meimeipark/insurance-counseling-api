package com.pym.insurancecounselingapi.controller;

import com.pym.insurancecounselingapi.entity.InsuranceCounseling;
import com.pym.insurancecounselingapi.model.InsuranceCounselingItem;
import com.pym.insurancecounselingapi.model.InsuranceCounselingRequest;
import com.pym.insurancecounselingapi.model.InsuranceCounselingResponse;
import com.pym.insurancecounselingapi.model.InsuranceCounselingStaticsResponse;
import com.pym.insurancecounselingapi.service.InsuranceCounselingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/insurance")
public class InsuranceCounselingController {
    private final InsuranceCounselingService insuranceCounselingService;

    @PostMapping("/new")
    public String setInsurance (@RequestBody InsuranceCounselingRequest request){
        insuranceCounselingService.setInsuranceCounseling(request);

        return "접수";
    }

    @GetMapping("/list")
    public List<InsuranceCounselingItem> getInsurances(){
        return insuranceCounselingService.getInsurances();
    }

    @GetMapping("detail/{id}")
    public InsuranceCounselingResponse getInsurance(@PathVariable long id){
       return insuranceCounselingService.getInsurance(id);
    }

    @GetMapping("/statics")
    public InsuranceCounselingStaticsResponse getStatics (){
        return insuranceCounselingService.getStatics();
    }
}
