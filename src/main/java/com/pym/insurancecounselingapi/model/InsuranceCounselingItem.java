package com.pym.insurancecounselingapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InsuranceCounselingItem {
    private Long id;
    private LocalDate counselingDay;
    private String clientName;
    private LocalDate birthDay;
    private String phoneNumber;
    private Boolean contract;
    private String diseaseCategory;
    private Boolean differentJoin;
}
