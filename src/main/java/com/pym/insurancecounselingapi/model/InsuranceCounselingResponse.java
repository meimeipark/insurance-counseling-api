package com.pym.insurancecounselingapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InsuranceCounselingResponse {
    private Long id;
    private LocalDate counselingDay;
    private String clientName;
    private LocalDate birthDay;
    private String phoneNumber;
    private String address;
    private String contract;
    private Double hopeMoney;
    private String diseaseCategory;
    private String join;
    private String differentJoin;
}
