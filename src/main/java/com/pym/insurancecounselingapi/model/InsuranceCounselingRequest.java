package com.pym.insurancecounselingapi.model;

import com.pym.insurancecounselingapi.enums.DiseaseCategory;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InsuranceCounselingRequest {
    private LocalDate counselingDay;
    private String clientName;
    private LocalDate birthDay;
    private String phoneNumber;
    private String address;
    private Boolean contract;
    private Double hopeMoney;
    @Enumerated(value = EnumType.STRING)
    private DiseaseCategory diseaseCategory;
    private Boolean differentJoin;
}
