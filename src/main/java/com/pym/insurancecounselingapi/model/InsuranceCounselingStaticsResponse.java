package com.pym.insurancecounselingapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InsuranceCounselingStaticsResponse {
    private Double avgHopePrice;
}
