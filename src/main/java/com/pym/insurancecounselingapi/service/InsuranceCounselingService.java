package com.pym.insurancecounselingapi.service;

import com.pym.insurancecounselingapi.entity.InsuranceCounseling;
import com.pym.insurancecounselingapi.model.InsuranceCounselingItem;
import com.pym.insurancecounselingapi.model.InsuranceCounselingRequest;
import com.pym.insurancecounselingapi.model.InsuranceCounselingResponse;
import com.pym.insurancecounselingapi.model.InsuranceCounselingStaticsResponse;
import com.pym.insurancecounselingapi.repository.InsuranceCounselingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InsuranceCounselingService {
    private final InsuranceCounselingRepository insuranceCounselingRepository;

    public void setInsuranceCounseling (InsuranceCounselingRequest request){
        InsuranceCounseling addData = new InsuranceCounseling();
        addData.setCounselingDay(request.getCounselingDay());
        addData.setClientName(request.getClientName());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setAddress(request.getAddress());
        addData.setContract(request.getContract());
        addData.setHopeMoney(request.getHopeMoney());
        addData.setDiseaseCategory(request.getDiseaseCategory());
        addData.setDifferentJoin(request.getDifferentJoin());

        insuranceCounselingRepository.save(addData);
    }

    public List<InsuranceCounselingItem> getInsurances(){
        List<InsuranceCounseling> orginList = insuranceCounselingRepository.findAll();

        List<InsuranceCounselingItem> result = new LinkedList<>();

        for (InsuranceCounseling insuranceCounseling : orginList){
            InsuranceCounselingItem addItem = new InsuranceCounselingItem();
            addItem.setId(insuranceCounseling.getId());
            addItem.setCounselingDay(insuranceCounseling.getCounselingDay());
            addItem.setClientName(insuranceCounseling.getClientName());
            addItem.setBirthDay(insuranceCounseling.getBirthDay());
            addItem.setPhoneNumber(insuranceCounseling.getPhoneNumber());
            addItem.setContract(insuranceCounseling.getContract());
            addItem.setDiseaseCategory(insuranceCounseling.getDiseaseCategory().getName());
            addItem.setDifferentJoin(insuranceCounseling.getDifferentJoin());

            result.add(addItem);
        }
        return result;
    }

    public InsuranceCounselingResponse getInsurance(long id){
        InsuranceCounseling originData = insuranceCounselingRepository.findById(id).orElseThrow();

        InsuranceCounselingResponse response = new InsuranceCounselingResponse();
        response.setId(originData.getId());
        response.setCounselingDay(originData.getCounselingDay());
        response.setClientName(originData.getClientName());
        response.setBirthDay(originData.getBirthDay());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setContract(originData.getContract() ? "설계사 상담 예약":"계약 실패");
        response.setHopeMoney(originData.getHopeMoney());
        response.setDiseaseCategory(originData.getDiseaseCategory().getName());
        response.setJoin(originData.getDiseaseCategory().getJoin() ? "가입가능":"가입불가");
        response.setDifferentJoin(originData.getDifferentJoin() ? "타 사 계약 내역 존재":"타 사 가입 내역 존재 안함");

        return response;
    }

    public InsuranceCounselingStaticsResponse getStatics(){
        InsuranceCounselingStaticsResponse staticsResponse = new InsuranceCounselingStaticsResponse();

        List<InsuranceCounseling> originList = insuranceCounselingRepository.findAll();

        double totalPrice = 0D;
        for (InsuranceCounseling insuranceCounseling : originList){
            totalPrice += insuranceCounseling.getHopeMoney();
        }

        double avgPrice = totalPrice / originList.size();

        staticsResponse.setAvgHopePrice(avgPrice);

        return staticsResponse;
    }
}
